// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Rodriguez_Game1/Rodriguez_Game1GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRodriguez_Game1GameMode() {}
// Cross Module References
	RODRIGUEZ_GAME1_API UEnum* Z_Construct_UEnum_Rodriguez_Game1_EGamePlayState();
	UPackage* Z_Construct_UPackage__Script_Rodriguez_Game1();
	RODRIGUEZ_GAME1_API UClass* Z_Construct_UClass_ARodriguez_Game1GameMode_NoRegister();
	RODRIGUEZ_GAME1_API UClass* Z_Construct_UClass_ARodriguez_Game1GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	RODRIGUEZ_GAME1_API UFunction* Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState();
// End Cross Module References
	static UEnum* EGamePlayState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Rodriguez_Game1_EGamePlayState, Z_Construct_UPackage__Script_Rodriguez_Game1(), TEXT("EGamePlayState"));
		}
		return Singleton;
	}
	template<> RODRIGUEZ_GAME1_API UEnum* StaticEnum<EGamePlayState>()
	{
		return EGamePlayState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGamePlayState(EGamePlayState_StaticEnum, TEXT("/Script/Rodriguez_Game1"), TEXT("EGamePlayState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Rodriguez_Game1_EGamePlayState_Hash() { return 1742845861U; }
	UEnum* Z_Construct_UEnum_Rodriguez_Game1_EGamePlayState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Rodriguez_Game1();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGamePlayState"), 0, Get_Z_Construct_UEnum_Rodriguez_Game1_EGamePlayState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGamePlayState::EPlaying", (int64)EGamePlayState::EPlaying },
				{ "EGamePlayState::EGameOver", (int64)EGamePlayState::EGameOver },
				{ "EGamePlayState::EUnknown", (int64)EGamePlayState::EUnknown },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Rodriguez_Game1GameMode.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Rodriguez_Game1,
				nullptr,
				"EGamePlayState",
				"EGamePlayState",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void ARodriguez_Game1GameMode::StaticRegisterNativesARodriguez_Game1GameMode()
	{
		UClass* Class = ARodriguez_Game1GameMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentState", &ARodriguez_Game1GameMode::execGetCurrentState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics
	{
		struct Rodriguez_Game1GameMode_eventGetCurrentState_Parms
		{
			EGamePlayState ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Rodriguez_Game1GameMode_eventGetCurrentState_Parms, ReturnValue), Z_Construct_UEnum_Rodriguez_Game1_EGamePlayState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Health" },
		{ "ModuleRelativePath", "Rodriguez_Game1GameMode.h" },
		{ "ToolTip", "Returns the current playing state" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARodriguez_Game1GameMode, nullptr, "GetCurrentState", sizeof(Rodriguez_Game1GameMode_eventGetCurrentState_Parms), Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARodriguez_Game1GameMode_NoRegister()
	{
		return ARodriguez_Game1GameMode::StaticClass();
	}
	struct Z_Construct_UClass_ARodriguez_Game1GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Rodriguez_Game1,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARodriguez_Game1GameMode_GetCurrentState, "GetCurrentState" }, // 132047003
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Rodriguez_Game1GameMode.h" },
		{ "ModuleRelativePath", "Rodriguez_Game1GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARodriguez_Game1GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::ClassParams = {
		&ARodriguez_Game1GameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x008802A8u,
		METADATA_PARAMS(Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARodriguez_Game1GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARodriguez_Game1GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARodriguez_Game1GameMode, 4163288911);
	template<> RODRIGUEZ_GAME1_API UClass* StaticClass<ARodriguez_Game1GameMode>()
	{
		return ARodriguez_Game1GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARodriguez_Game1GameMode(Z_Construct_UClass_ARodriguez_Game1GameMode, &ARodriguez_Game1GameMode::StaticClass, TEXT("/Script/Rodriguez_Game1"), TEXT("ARodriguez_Game1GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARodriguez_Game1GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
