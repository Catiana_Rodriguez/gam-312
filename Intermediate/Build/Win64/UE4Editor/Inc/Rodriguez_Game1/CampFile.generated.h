// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef RODRIGUEZ_GAME1_CampFile_generated_h
#error "CampFile.generated.h already included, missing '#pragma once' in CampFile.h"
#endif
#define RODRIGUEZ_GAME1_CampFile_generated_h

#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execApplyFireDamage) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ApplyFireDamage(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execApplyFireDamage) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ApplyFireDamage(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapEnd) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapEnd(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACampFile(); \
	friend struct Z_Construct_UClass_ACampFile_Statics; \
public: \
	DECLARE_CLASS(ACampFile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ACampFile)


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACampFile(); \
	friend struct Z_Construct_UClass_ACampFile_Statics; \
public: \
	DECLARE_CLASS(ACampFile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ACampFile)


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACampFile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACampFile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACampFile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACampFile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACampFile(ACampFile&&); \
	NO_API ACampFile(const ACampFile&); \
public:


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACampFile(ACampFile&&); \
	NO_API ACampFile(const ACampFile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACampFile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACampFile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACampFile)


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_PRIVATE_PROPERTY_OFFSET
#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_11_PROLOG
#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_RPC_WRAPPERS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_INCLASS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_INCLASS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RODRIGUEZ_GAME1_API UClass* StaticClass<class ACampFile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rodriguez_Game1_Source_Rodriguez_Game1_CampFile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
