// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RODRIGUEZ_GAME1_Rodriguez_Game1HUD_generated_h
#error "Rodriguez_Game1HUD.generated.h already included, missing '#pragma once' in Rodriguez_Game1HUD.h"
#endif
#define RODRIGUEZ_GAME1_Rodriguez_Game1HUD_generated_h

#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_RPC_WRAPPERS
#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARodriguez_Game1HUD(); \
	friend struct Z_Construct_UClass_ARodriguez_Game1HUD_Statics; \
public: \
	DECLARE_CLASS(ARodriguez_Game1HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ARodriguez_Game1HUD)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARodriguez_Game1HUD(); \
	friend struct Z_Construct_UClass_ARodriguez_Game1HUD_Statics; \
public: \
	DECLARE_CLASS(ARodriguez_Game1HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ARodriguez_Game1HUD)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARodriguez_Game1HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARodriguez_Game1HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARodriguez_Game1HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARodriguez_Game1HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARodriguez_Game1HUD(ARodriguez_Game1HUD&&); \
	NO_API ARodriguez_Game1HUD(const ARodriguez_Game1HUD&); \
public:


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARodriguez_Game1HUD(ARodriguez_Game1HUD&&); \
	NO_API ARodriguez_Game1HUD(const ARodriguez_Game1HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARodriguez_Game1HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARodriguez_Game1HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARodriguez_Game1HUD)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HUDWidgetClass() { return STRUCT_OFFSET(ARodriguez_Game1HUD, HUDWidgetClass); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(ARodriguez_Game1HUD, CurrentWidget); }


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_9_PROLOG
#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_RPC_WRAPPERS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_INCLASS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_INCLASS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RODRIGUEZ_GAME1_API UClass* StaticClass<class ARodriguez_Game1HUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
