// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGamePlayState : int32;
#ifdef RODRIGUEZ_GAME1_Rodriguez_Game1GameMode_generated_h
#error "Rodriguez_Game1GameMode.generated.h already included, missing '#pragma once' in Rodriguez_Game1GameMode.h"
#endif
#define RODRIGUEZ_GAME1_Rodriguez_Game1GameMode_generated_h

#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARodriguez_Game1GameMode(); \
	friend struct Z_Construct_UClass_ARodriguez_Game1GameMode_Statics; \
public: \
	DECLARE_CLASS(ARodriguez_Game1GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), RODRIGUEZ_GAME1_API) \
	DECLARE_SERIALIZER(ARodriguez_Game1GameMode)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_INCLASS \
private: \
	static void StaticRegisterNativesARodriguez_Game1GameMode(); \
	friend struct Z_Construct_UClass_ARodriguez_Game1GameMode_Statics; \
public: \
	DECLARE_CLASS(ARodriguez_Game1GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), RODRIGUEZ_GAME1_API) \
	DECLARE_SERIALIZER(ARodriguez_Game1GameMode)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	RODRIGUEZ_GAME1_API ARodriguez_Game1GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARodriguez_Game1GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RODRIGUEZ_GAME1_API, ARodriguez_Game1GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARodriguez_Game1GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RODRIGUEZ_GAME1_API ARodriguez_Game1GameMode(ARodriguez_Game1GameMode&&); \
	RODRIGUEZ_GAME1_API ARodriguez_Game1GameMode(const ARodriguez_Game1GameMode&); \
public:


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	RODRIGUEZ_GAME1_API ARodriguez_Game1GameMode(ARodriguez_Game1GameMode&&); \
	RODRIGUEZ_GAME1_API ARodriguez_Game1GameMode(const ARodriguez_Game1GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(RODRIGUEZ_GAME1_API, ARodriguez_Game1GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARodriguez_Game1GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARodriguez_Game1GameMode)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_PRIVATE_PROPERTY_OFFSET
#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_20_PROLOG
#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_RPC_WRAPPERS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_INCLASS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_INCLASS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RODRIGUEZ_GAME1_API UClass* StaticClass<class ARodriguez_Game1GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1GameMode_h


#define FOREACH_ENUM_EGAMEPLAYSTATE(op) \
	op(EGamePlayState::EPlaying) \
	op(EGamePlayState::EGameOver) \
	op(EGamePlayState::EUnknown) 

enum class EGamePlayState;
template<> RODRIGUEZ_GAME1_API UEnum* StaticEnum<EGamePlayState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
