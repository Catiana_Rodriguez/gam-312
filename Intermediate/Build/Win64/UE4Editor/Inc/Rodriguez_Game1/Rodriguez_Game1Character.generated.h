// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDamageType;
struct FVector;
class UPrimitiveComponent;
class AController;
class AActor;
struct FHitResult;
#ifdef RODRIGUEZ_GAME1_Rodriguez_Game1Character_generated_h
#error "Rodriguez_Game1Character.generated.h already included, missing '#pragma once' in Rodriguez_Game1Character.h"
#endif
#define RODRIGUEZ_GAME1_Rodriguez_Game1Character_generated_h

#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execReceivePointDamage) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_STRUCT(FVector,Z_Param_HitLocation); \
		P_GET_STRUCT(FVector,Z_Param_HitNormal); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_PROPERTY(UNameProperty,Z_Param_BoneName); \
		P_GET_STRUCT(FVector,Z_Param_ShotFromDirection); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitInfo); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReceivePointDamage(Z_Param_Damage,Z_Param_DamageType,Z_Param_HitLocation,Z_Param_HitNormal,Z_Param_HitComponent,Z_Param_BoneName,Z_Param_ShotFromDirection,Z_Param_InstigatedBy,Z_Param_DamageCauser,Z_Param_Out_HitInfo); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayFlash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->PlayFlash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicChange) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicChange(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicValue) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicValue(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetDamageState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetDamageState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDamageTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagicIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetMagicIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealthIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetHealthIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateMagic) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateMagic(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_HealthChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateHealth(Z_Param_HealthChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagic) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetMagic(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetHealth(); \
		P_NATIVE_END; \
	}


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execReceivePointDamage) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Damage); \
		P_GET_OBJECT(UDamageType,Z_Param_DamageType); \
		P_GET_STRUCT(FVector,Z_Param_HitLocation); \
		P_GET_STRUCT(FVector,Z_Param_HitNormal); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComponent); \
		P_GET_PROPERTY(UNameProperty,Z_Param_BoneName); \
		P_GET_STRUCT(FVector,Z_Param_ShotFromDirection); \
		P_GET_OBJECT(AController,Z_Param_InstigatedBy); \
		P_GET_OBJECT(AActor,Z_Param_DamageCauser); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitInfo); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReceivePointDamage(Z_Param_Damage,Z_Param_DamageType,Z_Param_HitLocation,Z_Param_HitNormal,Z_Param_HitComponent,Z_Param_BoneName,Z_Param_ShotFromDirection,Z_Param_InstigatedBy,Z_Param_DamageCauser,Z_Param_Out_HitInfo); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayFlash) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->PlayFlash(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicChange) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicChange(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetMagicValue) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetMagicValue(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetDamageState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetDamageState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execDamageTimer) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->DamageTimer(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagicIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetMagicIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealthIntText) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->GetHealthIntText(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateMagic) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_MagicChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateMagic(Z_Param_MagicChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_HealthChange); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateHealth(Z_Param_HealthChange); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMagic) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetMagic(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetHealth(); \
		P_NATIVE_END; \
	}


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARodriguez_Game1Character(); \
	friend struct Z_Construct_UClass_ARodriguez_Game1Character_Statics; \
public: \
	DECLARE_CLASS(ARodriguez_Game1Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ARodriguez_Game1Character)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_INCLASS \
private: \
	static void StaticRegisterNativesARodriguez_Game1Character(); \
	friend struct Z_Construct_UClass_ARodriguez_Game1Character_Statics; \
public: \
	DECLARE_CLASS(ARodriguez_Game1Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ARodriguez_Game1Character)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARodriguez_Game1Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARodriguez_Game1Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARodriguez_Game1Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARodriguez_Game1Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARodriguez_Game1Character(ARodriguez_Game1Character&&); \
	NO_API ARodriguez_Game1Character(const ARodriguez_Game1Character&); \
public:


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARodriguez_Game1Character(ARodriguez_Game1Character&&); \
	NO_API ARodriguez_Game1Character(const ARodriguez_Game1Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARodriguez_Game1Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARodriguez_Game1Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARodriguez_Game1Character)


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ARodriguez_Game1Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ARodriguez_Game1Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ARodriguez_Game1Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ARodriguez_Game1Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ARodriguez_Game1Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ARodriguez_Game1Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ARodriguez_Game1Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ARodriguez_Game1Character, L_MotionController); }


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_14_PROLOG
#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_RPC_WRAPPERS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_INCLASS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_INCLASS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RODRIGUEZ_GAME1_API UClass* StaticClass<class ARodriguez_Game1Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rodriguez_Game1_Source_Rodriguez_Game1_Rodriguez_Game1Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
