// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RODRIGUEZ_GAME1_CameraDirector_generated_h
#error "CameraDirector.generated.h already included, missing '#pragma once' in CameraDirector.h"
#endif
#define RODRIGUEZ_GAME1_CameraDirector_generated_h

#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_RPC_WRAPPERS
#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACameraDirector(); \
	friend struct Z_Construct_UClass_ACameraDirector_Statics; \
public: \
	DECLARE_CLASS(ACameraDirector, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ACameraDirector)


#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACameraDirector(); \
	friend struct Z_Construct_UClass_ACameraDirector_Statics; \
public: \
	DECLARE_CLASS(ACameraDirector, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(ACameraDirector)


#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACameraDirector(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACameraDirector) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraDirector); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraDirector); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraDirector(ACameraDirector&&); \
	NO_API ACameraDirector(const ACameraDirector&); \
public:


#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraDirector(ACameraDirector&&); \
	NO_API ACameraDirector(const ACameraDirector&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraDirector); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraDirector); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACameraDirector)


#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_PRIVATE_PROPERTY_OFFSET
#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_9_PROLOG
#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_RPC_WRAPPERS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_INCLASS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_INCLASS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RODRIGUEZ_GAME1_API UClass* StaticClass<class ACameraDirector>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rodriguez_Game1_Source_Rodriguez_Game1_CameraDirector_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
