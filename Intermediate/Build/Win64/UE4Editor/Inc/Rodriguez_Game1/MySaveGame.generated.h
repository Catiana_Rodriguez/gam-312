// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef RODRIGUEZ_GAME1_MySaveGame_generated_h
#error "MySaveGame.generated.h already included, missing '#pragma once' in MySaveGame.h"
#endif
#define RODRIGUEZ_GAME1_MySaveGame_generated_h

#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_RPC_WRAPPERS
#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMySaveGame(); \
	friend struct Z_Construct_UClass_UMySaveGame_Statics; \
public: \
	DECLARE_CLASS(UMySaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(UMySaveGame)


#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUMySaveGame(); \
	friend struct Z_Construct_UClass_UMySaveGame_Statics; \
public: \
	DECLARE_CLASS(UMySaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Rodriguez_Game1"), NO_API) \
	DECLARE_SERIALIZER(UMySaveGame)


#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMySaveGame(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMySaveGame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMySaveGame); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMySaveGame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMySaveGame(UMySaveGame&&); \
	NO_API UMySaveGame(const UMySaveGame&); \
public:


#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMySaveGame(UMySaveGame&&); \
	NO_API UMySaveGame(const UMySaveGame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMySaveGame); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMySaveGame); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMySaveGame)


#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_PRIVATE_PROPERTY_OFFSET
#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_11_PROLOG
#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_RPC_WRAPPERS \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_INCLASS \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_PRIVATE_PROPERTY_OFFSET \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_INCLASS_NO_PURE_DECLS \
	Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RODRIGUEZ_GAME1_API UClass* StaticClass<class UMySaveGame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rodriguez_Game1_Source_Rodriguez_Game1_MySaveGame_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
