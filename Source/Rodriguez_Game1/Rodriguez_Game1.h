// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Engine.h"
#include "CoreMinimal.h"
#include "MySaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "LightSwitchCodeOnly.h"
#include "LightSwitchTrigger.h"
#include "OnComponentHit.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.

