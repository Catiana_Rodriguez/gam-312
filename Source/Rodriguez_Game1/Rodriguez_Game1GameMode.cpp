// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Rodriguez_Game1GameMode.h"
#include "Rodriguez_Game1HUD.h"
#include "Rodriguez_Game1Character.h"
#include "Engine.h"
#include "UObject/ConstructorHelpers.h"

ARodriguez_Game1GameMode::ARodriguez_Game1GameMode()
	: Super()
{
	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ARodriguez_Game1HUD::StaticClass();
}
void ARodriguez_Game1GameMode::BeginPlay()
{
	Super::BeginPlay();

	SetCurrentState(EGamePlayState::EPlaying);

	MyCharacter = Cast<ARodriguez_Game1Character>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void ARodriguez_Game1GameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	GetWorld()->GetMapName();

	if (MyCharacter)
	{
		if (FMath::IsNearlyZero(MyCharacter->GetHealth(), 0.001f))
		{
			SetCurrentState(EGamePlayState::EGameOver);
		}
	}
}

EGamePlayState ARodriguez_Game1GameMode::GetCurrentState() const
{
	return CurrentState;
}

void ARodriguez_Game1GameMode::SetCurrentState(EGamePlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(CurrentState);
}

void ARodriguez_Game1GameMode::HandleNewState(EGamePlayState NewState)
{
	switch (NewState)
	{
	case EGamePlayState::EPlaying:
	{
		// do nothing
	}
	break;
	// Unknown/default state
	case EGamePlayState::EGameOver:
	{
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}
	break;
	// Unknown/default state
	default:
	case EGamePlayState::EUnknown:
	{
		// do nothing
	}
	break;
	}
}