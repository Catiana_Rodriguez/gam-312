

#include "OnComponentHit.h"
#include "Rodriguez_Game1.h"
#include "Components/BoxComponent.h"


AOnComponentHit::AOnComponentHit()
{
	
	PrimaryActorTick.bCanEverTick = true;

	
	MyComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	MyComp->SetSimulatePhysics(true);
	MyComp->SetNotifyRigidBodyCollision(true);

	MyComp->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	MyComp->OnComponentHit.AddDynamic(this, &AOnComponentHit::OnCompHit);

	
	RootComponent = MyComp;

}
void AOnComponentHit::BeginPlay()
{
	Super::BeginPlay();

}

void AOnComponentHit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AOnComponentHit::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("I Hit: %s"), *OtherActor->GetName()));
	}
}

